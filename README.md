# StarPay Backend Challenge #

Somos uma subadquirente, e atualmente, temos algumas soluções além de processamento de pagamentos de débito e crédito, como produtos de cashback e splits de pagamentos.
Nesse desafio, você precisará criar um modelo de um ambiente de transações simples, em qualquer linguagem de sua escolha.

### Schema
- Pessoa
    - A *Entidade Pessoa* (StarPay), é o nosso parceiro. Essa entidade, precisa armazenar dados iniciais a respeito do credenciado,

- Conta
    - A *Entidade Conta* guarda dados de domicílio bancário. Devem ser armazenados dados das contas bancárias de cada parceiro.

- Movimentação
    - A *Entidade Movimentação* agrupa os recebíveis desse parceiro a serem pagos (os valores a serem recebidos e valores já recebidos). O valor de repasse (que será pago), sempre será menor que o valor da transação, dado que serão realizados descontos de acordo com a bandeira em que foi realizada transação do cartão.

- Transação
    - A *Entidade Transação* que irá receber todos dados de cada transação. Nesse caso, precisamos identificar, através de um identificador único, cada transação, bem como valores, bandeiras e dados do cartão usado na transação.

### API
 
 - Entidade Pessoa
    - Criação de pessoa
    - Listagem com filtros
    - Edição 
    - Exclusão lógica/ Desativação
        - Regras de Negócio
            - Não pode aceitar cadastros para menores de 18 anos
            
 - Entidade Conta
    - Criação de contas
    - Listagem
    - Exclusão
        - Regras de negócio
            - Não pode excluir uma conta que já tenha sido movimentada (Entidade Movimentação), apenas exclusão lógica
            - Não podem existir duas contas iguais

 - Entidade Movimentação
    - Criação de movimento
    - Listagem com filtros (por data, valor, bandeira)
        - Regras de negócio
            - Cada bandeira possui um percentual de desconto diferente, então o valor de repasse, sempre será menor que o valor de transação. 
                - Ex: Uma transação de R$ 100,00 usando um cartão Mastercard, no crédito há um desconto de 2.8%, repassando no final R$ 97.20 e ficando para a operação R$ 2,80 a ser pago, 30 dias após a transação, caso a transação seja no dia 18/01/2020 o pagamento será o dia 20/02/2020, pois não efetuamos pagamentos em finais de semana e feriados.
                - Conforme lista abaixo:
                    - Mastercard Crédito - 2.8%
                    - Mastercard Débito - 4.3%
                    - Visa Crédito - 1.8%
                    - Visa Débito - 3.7%
            - Todo repasse de crédito, será efetuado em 30 a partir da data de transação
            - Todo repasse débito, será realizado 2 dias a partir da data de transação
 
 - Entidade Transação
    - Criação de transação
    - Listagem de transação (por data, valor, parceiro e bandeira)

Desafio extra:

- Atualmente, o ambiente de transações, está configurado em modo Domain Mode (WildFly), com um LoadBalancer que balanceia a carga para dois servidores em modo round robin. Porém, notamos que há um gargalo muito grande na entrada de transações, pois a quantidade de requisições recebidas. Como você lidaria com esse cenário, sabendo que precisamos escalar nossa solução?